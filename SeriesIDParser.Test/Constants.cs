﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeriesIDParser.Test
{
	public static class Constants
	{
		public const string Path = @"D:\Test";
		public const string MovieFilePath = "D:\\Test\\Der.Regenmacher.1997.German.1080p.BluRay.x264.mkv";
		public const string SeriesFilePath = "D:\\Test\\Gotham.S02E01.Glueck.oder.Wahrheit.1080p.BluRay.DUBBED.German.x264.mkv";
		public const string MovieFile = "Der.Regenmacher.1997.German.1080p.BluRay.x264.mkv";
		public const string SeriesFile = "Gotham.S02E01.Glueck.oder.Wahrheit.1080p.BluRay.DUBBED.German.x264.mkv";
	}
}
