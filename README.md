About this project
===================
**Now on [NuGet](https://www.nuget.org/packages/SeriesIDParser/)!**

This project is designed to receivs a series string like

    Knight.Rider.S01E07.Die.grosse.Duerre.1982.German.DVDRip.XviD-c0nFuSed.mkv

The output would be a object like that (Demo-App):

![enter image description here](https://projects.stefm.de/Gitlab/SeriesIDParser/images/DemoApp-ObjectStructure.png "Object-Structure")